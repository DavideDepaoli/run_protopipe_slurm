#!/bin/sh
#
#SBATCH -p long
#SBATCH -J build_energy
#SBATCH --mem=10g
#SBATCH -o log/log_%A.out
#
ulimit -l unlimited
ulimit -s unlimited
ulimit -a

echo ""
echo "================================================================================"
echo "==========   STEP 03 "
echo "================================================================================"
echo ""

source ./cfg_tmp.tmp

python "$script_dir/build_model.py" \
    --config_file ${cfg_regressor} \
    -i ${dl1_en_merged_dir} \
    --infile_signal ${DL1_en_gamma_merged} \
    -o ${energy_model_dir} \
    --cameras_from_config \
    --tail
