#!/bin/sh
#
#SBATCH -p short
#SBATCH -J merge_dl2
#SBATCH --mem=10g
#SBATCH -o log/log_%A_%a.out
#SBATCH --array=1-3
#
ulimit -l unlimited
ulimit -s unlimited
ulimit -a

echo ""
echo "================================================================================"
echo "==========   FILE: 08_merge_dl2.sh "
echo "================================================================================"
echo ""

source ./cfg_tmp.tmp

echo $SLURM_ARRAY_TASK_ID

case $SLURM_ARRAY_TASK_ID in

1)
    echo "gamma"
    python ./utils/merge.py --fmask "${dl2_dir}/${gamma_dl2_subdir}/*" \
        --merged_dir ${dl2_merged_dir} \
        --merged_fname ${DL2_gamma_merged} \
        --key "reco_events"
    ;;

2)
    echo "proton"
    python ./utils/merge.py --fmask "${dl2_dir}/${proton_dl2_subdir}*" \
        --merged_dir ${dl2_merged_dir} \
        --merged_fname ${DL2_proton_merged} \
        --key "reco_events"
    ;;

3)
    echo "electron"
    python ./utils/merge.py --fmask "${dl2_dir}/${electron_dl2_subdir}/*" \
        --merged_dir ${dl2_merged_dir} \
        --merged_fname ${DL2_electron_merged} \
        --key "reco_events"
    ;;

*)
    echo "Nothing to do"
    ;;
esac
