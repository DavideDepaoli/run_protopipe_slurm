import pandas as pd
import glob, os
import argparse

PARSER = argparse.ArgumentParser(
    description="Merge files", formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)

PARSER.add_argument(
    "-fm", "--fmask", type=str, required=True, help="File mask",
)
PARSER.add_argument(
    "-mdir", "--merged_dir", type=str, required=True, help="Merged dir",
)
PARSER.add_argument(
    "-mfn", "--merged_fname", type=str, required=True, help="Merged file name",
)
PARSER.add_argument(
    "-k", "--key", type=str, required=True, help="Key",
)


def merge_files(fmask, merged_dir, merged_fname, key):
    try:
        flist = sorted(
            glob.glob(fmask), key=lambda x: int(os.path.basename(x).split("_")[-2][3:]),
        )
        if len(flist) > 0:
            for i, f in enumerate(flist):
                print(f)
                if i == 0:
                    merged = pd.read_hdf(f, key=key)
                else:
                    merged = merged.append(pd.read_hdf(f, key=key))
            if not os.path.exists(merged_dir):
                os.mkdir(merged_dir)
            merged.to_hdf(f"{merged_dir}/{merged_fname}", key=key)
    except Exception as e:
        print(e)


if __name__ == "__main__":
    args = PARSER.parse_args()
    kwargs = args.__dict__
    merge_files(**kwargs)
