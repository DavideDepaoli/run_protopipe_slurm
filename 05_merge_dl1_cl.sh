#!/bin/sh
#
#SBATCH -p short
#SBATCH -J merge_dl2
#SBATCH --mem=10g
#SBATCH -o log/log_%A_%a.out
#SBATCH --array=1-2
#
ulimit -l unlimited
ulimit -s unlimited
ulimit -a

echo ""
echo "================================================================================"
echo "==========   STEP 05 "
echo "================================================================================"
echo ""

source ./cfg_tmp.tmp

echo $SLURM_ARRAY_TASK_ID

case $SLURM_ARRAY_TASK_ID in

1)
    echo "gamma"
    python ./utils/merge.py --fmask "${dl1_cl_dir}/${gamma_dl1_subdir}/*" \
        --merged_dir ${dl1_cl_merged_dir} \
        --merged_fname ${DL1_cl_gamma_merged} \
        --key "LSTCam"
    ;;

2)
    echo "proton"
    python ./utils/merge.py --fmask "${dl1_cl_dir}/${proton_dl1_subdir}/*" \
        --merged_dir ${dl1_cl_merged_dir} \
        --merged_fname ${DL1_cl_proton_merged} \
        --key "LSTCam"
    ;;

*)
    echo "Nothing to do"
    ;;
esac
