#!/bin/sh
#
#SBATCH -p short
#SBATCH -J reco_g_cl
#SBATCH --mem=5g
#SBATCH -o log/log_%A_%a.out
#
ulimit -l unlimited
ulimit -s unlimited
ulimit -a

echo ""
echo "================================================================================"
echo "==========   STEP 04 gamma "
echo "================================================================================"
echo ""

source ./cfg_tmp.tmp

run_start=$g_cl_run_start
div=$g_cl_div
# run_end = run_start + div * last_array

mkdir -p "${dl1_cl_dir}/${gamma_dl1_subdir}/"

for id in $(seq $((${div} - 1)) -1 0); do
    run=$(($SLURM_ARRAY_TASK_ID * ${div}))
    run=$(($run + ${run_start} - 1))
    run=$(($run - $id))

    echo ""
    echo "################################################################################"
    echo "##########   RUN $run "
    echo "################################################################################"
    echo ""

    python "${script_dir}/data_training.py" \
        --config_file ${cfg_analysis} \
        -i ${gamma_for_model_dl0_dir} \
        -f "${gamma_for_model_dl0_name_1}${run}${gamma_for_model_dl0_name_2}" \
        -o "${dl1_cl_dir}/${gamma_dl1_subdir}/${gamma_dl1_name_1}${run}${gamma_dl1_name_2}" \
        --estimate_energy True \
        --regressor_dir ${energy_model_dir} \
        --tail

done
