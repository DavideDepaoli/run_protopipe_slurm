#!/bin/sh
#
#SBATCH -p short
#SBATCH -J write_dl2_p
#SBATCH --mem=5g
#SBATCH -o log/log_%A_%a.out
#
ulimit -l unlimited
ulimit -s unlimited
ulimit -a

echo ""
echo "================================================================================"
echo "==========   FILE: 07_write_dl2_proton.sh "
echo "================================================================================"
echo ""

source ./cfg_tmp.tmp

run_start=$p_dl2_run_start
div=$p_dl2_div
# run_end = run_start + div * last_array

mkdir -p "${dl2_dir}/${proton_dl2_subdir}"

for id in $(seq $((${div} - 1)) -1 0); do
    run=$(($SLURM_ARRAY_TASK_ID * ${div}))
    run=$(($run + ${run_start} - 1))
    run=$(($run - $id))

    echo ""
    echo "################################################################################"
    echo "##########   RUN $run "
    echo "################################################################################"
    echo ""

    python "$script_dir/write_dl2.py" \
        --config_file ${cfg_analysis} \
        -i ${proton_dl0_dir} \
        -f "${proton_dl0_name_1}${run}${proton_dl0_name_2}" \
        -o "${dl2_dir}/${proton_dl2_subdir}/${proton_dl2_name_1}${run}${proton_dl2_name_2}" \
        --regressor_dir ${energy_model_dir} \
        --classifier_dir ${classifier_model_dir} \
        --tail

done
