#!/bin/sh
#
#SBATCH -p short
#SBATCH -J reco_g_en
#SBATCH --mem=5g
#SBATCH -o log/log_%A_%a.out
#
ulimit -l unlimited
ulimit -s unlimited
ulimit -a

echo ""
echo "================================================================================"
echo "==========   STEP 01 "
echo "================================================================================"
echo ""

source ./cfg_tmp.tmp

run_start=$g_en_run_start
div=$g_en_div
# run_end = run_start + div * last_array
echo "${dl1_en_dir}/${gamma_dl1_subdir}/"
mkdir -p "${dl1_en_dir}/${gamma_dl1_subdir}/"

for id in $(seq $((${div} - 1)) -1 0); do
    run=$(($SLURM_ARRAY_TASK_ID * ${div}))
    run=$(($run + ${run_start} - 1))
    run=$(($run - $id))

    echo ""
    echo "################################################################################"
    echo "##########   RUN $run "
    echo "################################################################################"
    echo ""

    mkdir -p "${dl1_en_dir}/${gamma_dl1_subdir}/"

    python "${script_dir}/data_training.py" \
        --config_file ${cfg_analysis} \
        -i ${gamma_for_model_dl0_dir} \
        -f "${gamma_for_model_dl0_name_1}${run}${gamma_for_model_dl0_name_2}" \
        -o "${dl1_en_dir}/${gamma_dl1_subdir}/${gamma_dl1_name_1}${run}${gamma_dl1_name_2}" \
        --tail

done
