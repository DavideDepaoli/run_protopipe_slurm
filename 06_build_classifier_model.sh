#!/bin/sh
#
#SBATCH -p long
#SBATCH -J build_classifier
#SBATCH --mem=10g
#SBATCH -o log/log_%A.out
#
ulimit -l unlimited
ulimit -s unlimited
ulimit -a

echo ""
echo "================================================================================"
echo "==========   STEP 06 "
echo "================================================================================"
echo ""

source ./cfg_tmp.tmp

python "$script_dir/build_model.py" \
    --config_file ${cfg_classifier} \
    -i ${dl1_cl_merged_dir} \
    --infile_signal ${DL1_cl_gamma_merged} \
    --infile_background ${DL1_cl_proton_merged} \
    -o ${classifier_model_dir} \
    --cameras_from_config \
    --tail
