#!/bin/sh
#
#SBATCH -p short
#SBATCH -J merge_dl1
#SBATCH --mem=10g
#SBATCH -o log/log_%A.out
#
ulimit -l unlimited
ulimit -s unlimited
ulimit -a

echo ""
echo "================================================================================"
echo "==========   STEP O2 "
echo "================================================================================"
echo ""

source ./cfg_tmp.tmp

echo "gamma"
python "./utils/merge.py" \
    --fmask "${dl1_en_dir}${gamma_dl1_subdir}/*" \
    --merged_dir ${dl1_en_merged_dir} \
    --merged_fname ${DL1_en_gamma_merged} \
    --key "LSTCam"
