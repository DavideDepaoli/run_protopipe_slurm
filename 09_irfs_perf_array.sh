#!/bin/sh
#
#SBATCH -p short
#SBATCH -J irfs
#SBATCH --mem=10g
#SBATCH -o log/log_%A_%a.out
#
ulimit -l unlimited
ulimit -s unlimited
ulimit -a

echo ""
echo "================================================================================"
echo "==========   FILE: 09_irfs_array.sh "
echo "================================================================================"
echo ""

source ./cfg_tmp.tmp

python "$script_dir/make_performance.py" \
    --config_file "${cfg_dir}/performance/performance_${SLURM_ARRAY_TASK_ID}.yaml" \
    --tail
