import subprocess, time, yaml
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("first", nargs="?", default=0)
parser.add_argument("last", nargs="?", default=9)
args = parser.parse_args()

first = int(args.first)
last = int(args.last)


cfg = yaml.load(open("../config/pipeline.yaml"), Loader=yaml.FullLoader)


def run_process(cmd):
    pr = subprocess.Popen(cmd, stdout=subprocess.PIPE, text=True)
    out, err = pr.communicate()


def wait_process_running():
    cmd_queue = ["squeue", "-u", cfg["user_name"]]
    process_running = True
    while process_running:
        pr_q = subprocess.Popen(cmd_queue, stdout=subprocess.PIPE, text=True)
        out_q, err_q = pr_q.communicate()
        print(out_q)
        print(f"len = {len(out_q)}")
        if len(out_q) < 90:
            process_running = False
        time.sleep(10)


run_process(["bash", "00_init.sh"])

if first <= 1 and last >= 1:
    print("Running step 01")
    run_process(["sbatch", f"--array=1-{cfg['g_en_arr_high']}", "01_reco_gamma_en.sh"])
    wait_process_running()
    print("Finished step 01")

if first <= 2 and last >= 2:
    print("Running step 02")
    run_process(["sbatch", "02_merge_dl1_en.sh"])
    wait_process_running()
    print("Finished step 02")

if first <= 3 and last >= 3:
    print("Running step 03")
    run_process(["sbatch", "03_build_energy_model.sh"])
    wait_process_running()
    print("Finished step 03")

if first <= 4 and last >= 4:
    print("Running step 04")
    run_process(["sbatch", f"--array=1-{cfg['g_cl_arr_high']}", "04_reco_gamma_cl.sh"])
    run_process(["sbatch", f"--array=1-{cfg['p_cl_arr_high']}", "04_reco_proton_cl.sh"])
    wait_process_running()
    print("Finished step 04")

if first <= 5 and last >= 5:
    print("Running step 05")
    run_process(["sbatch", "05_merge_dl1_cl.sh"])
    wait_process_running()
    print("Finished step 05")

if first <= 6 and last >= 6:
    print("Running step 06")
    run_process(["sbatch", "06_build_classifier_model.sh"])
    wait_process_running()
    print("Finished step 06")

if first <= 7 and last >= 7:
    print("Running step 07")
    run_process(
        ["sbatch", f"--array=1-{cfg['g_dl2_arr_high']}", "07_write_dl2_gamma.sh"]
    )
    run_process(
        ["sbatch", f"--array=1-{cfg['p_dl2_arr_high']}", "07_write_dl2_proton.sh"]
    )
    run_process(
        ["sbatch", f"--array=1-{cfg['e_dl2_arr_high']}", "07_write_dl2_electron.sh"]
    )
    wait_process_running()
    print("Finished step 07")

if first <= 8 and last >= 8:
    print("Running step 08")
    run_process(["sbatch", "08_merge_dl2.sh"])
    wait_process_running()
    print("Finished step 08")

if first <= 9 and last >= 9:
    print("Running step 09")
    run_process(
        ["sbatch", f"--array=1-{cfg['irfs_arr_high']}", "09_irfs_perf_array.sh"]
    )
    print("Finished step 09")
